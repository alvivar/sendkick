$(document).ready(function()
{

	// Show the edit options on over

	$(".ticket").hover(function()
	{
		$(this).find(".avatar").hide();
		$(this).find(".btnReloadCoupon").show();
	}, function()
	{
		$(this).find(".btnReloadCoupon").hide();
		$(this).find(".avatar").show();
	});
	// Clean all fields from edit/new coupon

	function cleanEditCouponView()
	{
		$(".frmErrors").html("");
		$('#dlgEditCoupon input[name$="_id"]').val("");
		$('#dlgEditCoupon input[name$="name"]').val("");
		$('#dlgEditCoupon textarea[name$="description"]').val("");
		$('#dlgEditCoupon input[name$="old_price"]').val("");
		$('#dlgEditCoupon input[name$="new_price"]').val("");
		$('#dlgEditCoupon input[name$="limit"]').val("");
		$('#dlgEditCoupon input[name$="expiration"]').val("");
		$('#dlgEditCoupon input[name$="tags"]').val("");
		$('#dlgEditCoupon textarea[name$="rules"]').val("");
	}

	// Edit profile form

	$("#btnEditProfile").click(function()
	{

		$("#dlgEditProfile").dialog(
		{
			modal : true,
			minWidth : 550,
			resizable : false
		});

		// AJAX submit

		var options =
		{
			target : '#dlgEditProfile .frmErrors',
			beforeSubmit : dlgEditProfileBefore,
			success : dlgEditProfileAfter
		};

		$('#frmEditProfile').submit(function()
		{
			$(this).ajaxSubmit(options);

			return false;
		});

		function dlgEditProfileBefore(formData, jqForm, options)
		{
			$("#dlgEditProfile .frmErrors").html($("#lostAndFound").html());
		}

		function dlgEditProfileAfter(responseText, statusText, xhr, $form)
		{
			if(responseText.length <= 0)
			{
				window.location = "";
			}
		}

	});
	// New coupon

	$("#btnEditCoupon").click(function()
	{
		cleanEditCouponView();
		openEditCouponView();
	});
	function openEditCouponView()
	{

		resetDeleteBtn();

		$("#dlgEditCoupon").dialog(
		{
			modal : true,
			minWidth : 975,
			resizable : false
		});

		$("#datepicker").datepicker(
		{
			minDate : 0
			// dateFormat: 'yy-mm-dd'
		});

		// AJAX submit

		var options =
		{
			target : '#dlgEditCoupon .frmErrors',
			beforeSubmit : dlgEditCouponBefore,
			success : dlgEditCouponAfter
		};

		$('#frmEditCoupon').submit(function()
		{
			$(this).ajaxSubmit(options);

			return false;
		});
		function dlgEditCouponBefore(formData, jqForm, options)
		{
			$("#dlgEditCoupon .frmErrors").html($("#lostAndFound").html());
		}

		function dlgEditCouponAfter(responseText, statusText, xhr, $form)
		{
			if(responseText.length <= 0)
			{
				window.location = "";
			}
		}

	}

	// Edit coupon
	// Reload the form with the JSON values on a hidden div.

	$(".btnReloadCoupon").click(function()
	{

		var storage = $(this).parent().parent().find(".jsonMessage");
		var data = jQuery.parseJSON(storage.html());

		$('#dlgEditCoupon input[name$="_id"]').val(data._id);
		$('#dlgEditCoupon input[name$="name"]').val(data.name);
		$('#dlgEditCoupon textarea[name$="description"]').val(data.description);
		$('#dlgEditCoupon input[name$="old_price"]').val(data.old_price);
		$('#dlgEditCoupon input[name$="new_price"]').val(data.new_price);
		$('#dlgEditCoupon input[name$="limit"]').val(data.limit);
		$('#dlgEditCoupon input[name$="expiration"]').val(data.expiration);
		$('#dlgEditCoupon input[name$="tags"]').val(data.tags);
		$('#dlgEditCoupon textarea[name$="rules"]').val(data.rules);

		openEditCouponView();
	});
	// Delete coupon

	function resetDeleteBtn()
	{
		$(".btnDeleteCouponLock").show();
		$(".btnDeleteCoupon").hide();
		$(".btnDeleteCouponCancel").hide();
	}


	$(".btnDeleteCouponLock").click(function()
	{
		$(this).hide();
		$(this).parent().find(".btnDeleteCoupon").fadeIn("fast");
		$(this).parent().find(".btnDeleteCouponCancel").fadeIn("fast");
	});

	$(".btnDeleteCouponCancel").click(function()
	{
		$(this).parent().find(".btnDeleteCouponLock").fadeIn("fast");
		$(this).parent().find(".btnDeleteCoupon").hide();
		$(this).hide();
	});

	$(".btnDeleteCoupon").click(function()
	{
		var data = $(this).parent().find('input[name$="_id"]');
		$.post("brands/ajaxDeleteCoupon",
		{
			_id : data.val()
		}, function(msg)
		{
			window.location = "";
		});
	});
	// Validate code

	$("#btnValidateCode").click(function()
	{

		$("#dlgValidateCode").dialog(
		{
			modal : true,
			minWidth : 550,
			resizable : false
		});

		// AJAX submit

		var options =
		{
			target : '#dlgValidateCode .frmErrors',
			beforeSubmit : dlgValidateCodeBefore,
			success : dlgValidateCodeAfter
		};

		$('#frmValidateCode').submit(function()
		{
			$(this).ajaxSubmit(options);

			return false;
		});
		function dlgValidateCodeBefore(formData, jqForm, options)
		{
			$("#dlgValidateCode .frmErrors").html($("#lostAndFound").html());
		}

		function dlgValidateCodeAfter(responseText, statusText, xhr, $form)
		{
			if(responseText.indexOf("!") != -1)
			{
				$('#dlgValidateCode a').attr("href", "/keys/consume/" + $('#dlgValidateCode input[name$="key"]').val());
				$('#dlgValidateCode a').show();
			}
			else
			{
				$('#dlgValidateCode a').hide();
			}
		}

	});
});
