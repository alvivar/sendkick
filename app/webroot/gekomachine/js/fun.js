$(document).ready(function() {

    // The show begin

    $("body").fadeIn("fast");

    $(".textContainer").hide();

    $("#weContent").show();
    
    // The click show content fuck shit

    $("#buttons a").click(function() {

        // Id

        var id  = $(this).attr("id");

        var contentId = "#" + id + "Content";

        // Hide for show

        $(".textContainer").hide();

        $(".textTitle").hide();

        $(contentId + " p").hide();

        // Show

        $(contentId).show();

        $(".textTitle").show("fast", function() {

            $(contentId + " p").fadeIn("slow");

        });

    });

    // The orange hover in the buttons

    $("#buttons a").hover(function() {

        $(this).parent().css("background-color", "#F8C73C");

    }, function() {

        $(this).parent().css("background-color", "#55B9DF");

    });
    
    // The over in the social buttons

    // TWITTER
    
    $("#twitterLogo").hover(function() {
       
        $(this).attr("src", "img/social/twitter_over.png");

    }, function() {
       
        $(this).attr("src", "img/social/twitter.png");
       
    });

    // FACEBOOK

    $("#facebookLogo").hover(function() {

        $(this).attr("src", "img/social/facebook_over.png");

    }, function() {

        $(this).attr("src", "img/social/facebook.png");

    });

    // LINKED IN

    $("#linkedinLogo").hover(function() {

        $(this).attr("src", "img/social/linkedin_over.png");

    }, function() {

        $(this).attr("src", "img/social/linkedin.png");

    });

    // Image preloading

    (function($) {
        var cache = [];
        $.preLoadImages = function() {
            var args_len = arguments.length;
            for (var i = args_len; i--;) {
                var cacheImage = document.createElement('img');
                cacheImage.src = arguments[i];
                cache.push(cacheImage);
            }
        }
    })(jQuery)

    $.preLoadImages(
        "img/social/twitter_over.png",
        "img/social/facebook_over.png",
        "img/social/linkedin_over.png");

});