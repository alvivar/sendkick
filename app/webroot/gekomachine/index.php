<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <title>gekomachine.com / Web + Mobile apps</title>
        <link rel="shortcut icon" href="gekoicon.jpg">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="google-site-verification" content="cJVtoz7lO37hgEcRWcf2glyDBUBvW6xqeUXm1Scjepc" />
        <link href="style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="js/jquery.js"> </script>
        <script type="text/javascript" src="js/fun.js"> </script>
    </head>
    <!-- 
    INTRUDER ALERT!
    Broma.
    -->
    <body style="display: none;">
        <?php // phpinfo(); ?>
        <div id="coolBg">&nbsp;</div>
        <div id="like">
            <iframe 
                src="http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.facebook.com%2FGekoMachine&amp;layout=box_count&amp;show_faces=true&amp;width=0&amp;action=like&amp;colorscheme=light&amp;height=65" 
                scrolling="no" 
                frameborder="0" 
                style="border:none; overflow:hidden; height:65px;" 
                allowTransparency="true">
            </iframe>        
        </div>
        <div id="plus1">
            <g:plusone size="tall"></g:plusone>
            <script type="text/javascript">
                (function() {
                    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                    po.src = 'https://apis.google.com/js/plusone.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                })();
            </script>
        </div>
        <div id="universe">
            <div id="header">
                &nbsp;
            </div>
            <div id="content" class="webPadding">
                <div id="logo">
                    <img id="mainLogo" src="img/logos/img_logo_gekomachine.png" alt="gekomachine.com"/>
                </div>
                <div id="buttons">
                    <ul>
                        <li><a id="we" href="#GekoMachine">&iquest;GekoMachine?</a></li>
                        <!--
                        <li><a id="will" href="#">Spoiler Alert</a></li>
                        -->
                        <li><a id="conquer" href="#GekoLabs">GekoLabs</a></li>
                        <!--
                        <li><a id="the" href="#the">Info</a></li>
                        <li><a id="world" href="#world">Contacto</a></li>
                        -->
                    </ul>
                </div>
                <div id="text">
                    <div id="weContent" class="textContainer">
                        <span class="textTitle">
                            GekoMachine
                        </span>
                        <p>
                            <span class="minimal_quote">&lt;clich&eacute;&gt;</span>Somos una empresa joven, fresca y 
                            llena de ideas.<span class="minimal_quote">&lt;/clich&eacute;&gt;</span>
                        </p>
                        <p>
                            La verdad es que nos apasiona Internet, en 
                            GekoMachine queremos inventar, experimentar, 
                            crear para Internet y plataformas m&oacute;biles.
                        </p>
                        <p>
                            Invitados a seguirnos en
                            <a href="http://twitter.com/gekomachine" alt="GekoMachine Twitter">Twitter</a>, 
                            <a href="http://www.facebook.com/GekoMachine" alt="GekoMachine Facebook">Facebook</a> o
                            <a href="http://www.linkedin.com/companies/gekomachine?trk=fc_badge" alt="GekoMachine LinkedIn">LinkedIn</a>.
                        </p>
                    </div>
                    <div id="willContent" class="textContainer">
                        <span class="textTitle">
                            &nbsp;
                        </span>
                        <p>
                            :S
                        </p>
                    </div>
                    <div id="conquerContent" class="textContainer">
                        <span class="textTitle">
                            GekoLabs
                        </span>
                        <p>
                            &nbsp;
                        </p>
                        <p>                            
                            <a href="http://www.sendkick.com">SendKick</a>: Una plataforma de promociones y marketing 
                            viral para productos y marcas <a href="http://www.sendkick.com" class="minimal_quote">[ir]</a>. 
                        </p>
                    </div>
                    <div id="theContent" class="textContainer">
                        <span class="textTitle">
                            &nbsp;
                        </span>
                        <p>
                            :P
                        </p>
                    </div>
                    <div id="worldContent" class="textContainer">
                        <span class="textTitle">
                            null pointer exception
                        </span>
                        <p>
                            :O
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div id="footer" class="webPadding">
            <div id="socialBar">
                <img id="secondLogo" src="img/logos/img_gekosocial.png" alt="Social"/>
                <div style="clear: both;"></div>
                <ul>
                    <li>
                        <a href="http://twitter.com/GekoMachine"><img id="twitterLogo" src="img/social/twitter.png" alt="Twitter"/></a>
                    </li>
                    <li>
                        <a href="http://www.facebook.com/GekoMachine"><img id="facebookLogo" src="img/social/facebook.png" alt="Facebook"/></a>
                    </li>
                    <li>
                        <a href="http://www.linkedin.com/companies/gekomachine?trk=fc_badge" target="_cool"><img id="linkedinLogo" src="img/social/linkedin.png" alt="LinkedIn"/></a>
                    </li>
                </ul>
            </div>
            <div id="contact">
                Gekomachine S.A. |
                Contacto: <a href="mailto:info@gekomachine.com">info@gekomachine.com</a> | 
                Costa Rica
            </div>
        </div>
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-17835221-1']);
            _gaq.push(['_setDomainName', '.gekomachine.com']);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();

        </script>
    </body>
</html>