<?php

namespace app\controllers;

use app\models\Coupons;
use app\models\Brands;
use app\models\Tags;
use lithium\util\Inflector;
use li3_flash_message\extensions\storage\FlashMessage;

class CouponsController extends \lithium\action\Controller
{

    public $publicActions = array(
        'show',
        'search'
    );

    public function show($brand, $slug)
    {
    	$coupon = Coupons::find('first', array(
                    'conditions' => array(
                        'brand' => $brand,
                        'slug' => $slug
                    )
                ));

        if (!$coupon)
        {
            FlashMessage::write('No encontramos ese cupón!');

            $this->redirect('/');
        }

        $coupons = Coupons::find('all', array(
                    'conditions' => array(
                        'brand' => $brand
                    ),
                    'limit' => 10
                ));

        return compact('coupon', 'coupons');
    }

    public function search()
    {
        if (!isset($this->request->query['q']))
        {
            return FALSE;
        }
        
        // Normalize query for search

        $query = strtolower(Inflector::slug($this->request->query['q']));

        // Register the search
        
        Tags::pingSearch($query);
        
        // Search

        $coupons = Coupons::find('all', array(
                    'conditions' => array(
                        'tags_search' => array(
                            '$in' => array(
                                $query
                            )
                        )
                    ),
                    'limit' => 20
                ));

        return compact('coupons');
    }

}

?>