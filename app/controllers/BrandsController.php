<?php

	namespace app\controllers;

	use app\models\Brands;
	use app\models\Coupons;
	use app\models\Keys;
	use app\extensions\services\S3;
	use lithium\security\Auth;
	use lithium\storage\Session;
	use lithium\util\Validator;
	use li3_flash_message\extensions\storage\FlashMessage;

	use li3_swiftmailer\mailer\Transports;
	use li3_swiftmailer\mailer\Message;

	class BrandsController extends \lithium\action\Controller
	{

		public $publicActions = array(
				'login',
				'register',
				'show',
				'lost',
				'recover'
		);

		public function login()
		{
			// If 0 request, do nothing weird

			if (!$this->request->data)
			{
				$brand = Brands::create();

				return compact('brand');
			}

			// If everything is ok, save the user in session

			$authBrand = Auth::check('default', $this->request);

			if ($authBrand)
			{
				unset($authBrand['password']);

				Session::write('sendkick', $authBrand);

				return $this->redirect('/brands');
			}

			// Data validation

			$brand = Brands::create($this->request->data);

			$rules = array(
					'username' => array( array(
								'usernameExists',
								'message' => 'Este nombre de usuario no se encuentra registrado en SendKick!'
						)),
					'password' => array( array(
								'usernameVsPass',
								'message' => 'Contraseña equivocada!'
						))
			);

			$brand->validates(array('rules' => $rules));

			return compact('brand');
		}

		public function logout()
		{
			Auth::clear('default');

			Session::delete('sendkick');

			return $this->redirect('/brands/login');
		}

		public function lost()
		{
			$brand = Brands::create();

			if ($this->request->data)
			{
				$brand = Brands::create($this->request->data);
				$rules = array('email' => array(
							array(
									'email',
									'message' => 'Tu email está mal escrito!'
							),
							array(
									'emailExists',
									'message' => 'Tu email no está registrado en SendKick!'
							)
					));
				$brand->validates(array('rules' => $rules));
				if (!$brand->errors())
				{
					$weirdKey = md5($this->request->data['email'] . SENDKICK_EASY_SALT . date('d'));
					$sendkickEmail = SENDKICK_EMAIL;
					
					$mailer = Transports::adapter('default');
					$brand = Brands::create($this->request->data); // #todo Check if needed
					$subject = 'SendKick - Recupera tu password';
					$from = array(SENDKICK_EMAIL => SENDKICK_NAME);
					$to = array($this->request->data['email'] => $this->request->data['email']);
					$body = "
¡Hola!

Has recibido este email porque estás solicitando tu contraseña. De ser así clickea en el siguiente link para validar tu acción:

http://sendkick.com/brands/recover/{$weirdKey}/{$this->request->data['email']}

Si no has solicitado tu contraseña comúnicate con nosotros acá: {$sendkickEmail}

Alguien podría estar intentando recuperarla por ti :O

¡Que la pases bien!
";

					$message = Message::newInstance()->setSubject($subject)->setFrom($from)->setTo($to)->setBody(trim($body));
					$valueMail = $mailer->send($message);
					FlashMessage::write("Revisa tu email (y tu folder de spam, nunca se sabe), hemos enviado una nueva contraseña!");
				}
				else
				{
					FlashMessage::write("Hay un error, verifica tus datos!");
				}
			}

			$brand->email = '';

			return compact('brand');
		}

		public function recover($token, $email)
		{
			$testKey = md5($email . SENDKICK_EASY_SALT . date('d'));

			if ($token == $testKey)
			{
				$newPass = Brands::changePass($email);

				$mailer = Transports::adapter('default');
				$brand = Brands::create($this->request->data);
				$subject = 'SendKick - Tu nueva contraseña';
				$from = array(SENDKICK_EMAIL => SENDKICK_NAME);
				$to = array($email => $email);
				$body = "
¡Hola!

Esta es tu nueva contraseña: {$newPass}

Te recomendamos cambiarla apenas ingreses a tu perfil.

¡Que la pases bien!
";

				$message = Message::newInstance()->setSubject($subject)->setFrom($from)->setTo($to)->setBody(trim($body));
				$valueMail = $mailer->send($message);

				FlashMessage::write("Hemos validado el cambio de contraseña. Revisa tu correo nuevamente!");
			}
			else
			{
				FlashMessage::write("Este enlace ha caducado y ya no es válido.");
			}

			$this->redirect('/');
		}

		public function register()
		{
			if (($this->request->data) && $brand->save())
			{
				$new_user = $this->request->data['username'];

				FlashMessage::write("{$new_user} ha sido registrado satisfactoriamente!");

				$mailer = Transports::adapter('default');

				$brand = Brands::create($this->request->data);

				$subject = 'Bienvenido a SendKick!';

				$from = array('sendkick@gekomachine.com' => 'GekoMachine');

				$to = array('andresalvivar@gmail.com' => 'Don Andrés');

				$body = "
Saludos! Bienvenido a SendKick.

Ante cualquier duda blablabla...
";

				$message = Message::newInstance()->setSubject($subject)->setFrom($from)->setTo($to)->setBody(trim($body));

				$valueMail = $mailer->send($message);

				return $this->redirect('/brands/login');
			}
			else
			{
				FlashMessage::write("Error registrando usuario");
			}

			return compact('brand');
		}

		// Test
		// This function need to be destroyed.
		public function reload()
		{
			$authBrand = Session::read('sendkick');

			for ($i = 5; $i > 0; $i--)
			{
				$lipsum = "lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum mattis libero et sodales pellentesque sapien tortor fermentum arcu non fermentum mi lectus non urna nam commodo convallis consectetur donec venenatis pellentesque nunc ut placerat velit pretium id proin quam turpis lacinia et gravida vel sodales aliquam turpis ut dapibus dolor dictum turpis interdum blandit etiam et erat sed augue condimentum elementum donec egestas diam a urna consectetur feugiat quisque cursus bibendum odio id varius metus aliquam at maecenas vitae eros id odio rhoncus viverra nunc eleifend posuere leo nec venenatis turpis convallis eu lorem ipsum dolor sit amet consectetur adipiscing elit morbi commodo ornare gravida quisque vitae nibh vel erat vehicula malesuada phasellus pretium sodales bibendum donec dignissim elit a augue auctor ac fermentum turpis gravida aliquam erat volutpat nam pharetra sem imperdiet urna condimentum ut molestie neque aliquam suspendisse";
				$words = explode(' ', $lipsum);
				shuffle($words);

				$lolData = array();
				$lolData['name'] = "{$words[11]} {$words[12]} {$words[13]}";
				$lolData['description'] = "{$words[14]} {$words[15]} {$words[16]} {$words[17]} {$words[18]} {$words[14]} {$words[15]} {$words[16]} {$words[17]} {$words[18]} {$words[14]} {$words[15]} {$words[16]} {$words[17]} {$words[18]}";
				$lolData['old_price'] = mt_rand(2500, 5000);
				$lolData['new_price'] = mt_rand(1000, 2500);
				$lolData['limit'] = mt_rand(5, 10);
				$lolData['expiration'] = '08/31/2011';
				$lolData['tags'] = "{$words[3]}, {$words[4]}, {$words[5]}, {$words[6]}";
				$lolData['rules'] = "{$words[7]} {$words[8]} {$words[9]} {$words[10]} {$words[7]} {$words[8]} {$words[9]} {$words[10]} {$words[7]} {$words[8]} {$words[9]} {$words[10]} {$words[7]} {$words[8]} {$words[9]} {$words[10]}";

				$coupon = Coupons::newCoupon($authBrand['username'], $lolData);
			}

			FlashMessage::write('BETA reload() Done!');

			$this->redirect('/brands');
		}

		// The brand admin.
		public function index()
		{
			$authBrand = Session::read('sendkick');

			$brand = Brands::first($authBrand['_id']);

			$coupons = Coupons::find('all', array('conditions' => array('brand' => $authBrand['username'])));

			return compact('brand', 'coupons');
		}

		// The brand public profile.
		public function show($username = null)
		{
			if (!$username)
			{
				$this->redirect('/');
			}

			$brand = Brands::find('first', array('conditions' => array('username' => $username)));

			if (!$brand)
			{
				FlashMessage::write('Esa cuenta no existe!');

				$this->redirect('/');
			}

			$coupons = Coupons::find('all', array('conditions' => array('brand' => $username)));

			return compact('brand', 'coupons');
		}

		// @todo This fuctions may or not may be here.
		// Transform errors() into a html simple list.
		protected function errorsToList($errors)
		{
			$list = '<ul>';

			foreach ($errors as $error)
			{
				foreach ($error as $e)
				{

					$list .= "<li>{$e}</li>";
				}
			}

			$list .= '</ul>';

			return $list;
		}

		// Edit the current user profile, returns html with errors or nothing on
		// save.
		public function ajaxEditProfile()
		{
			$this->_render['auto'] = false;

			$authBrand = Session::read('sendkick');

			if (isset($this->request->data['file']) && intval($this->request->data['file']['size']) > 0)
			{
				$file = $this->request->data['file'];

				$s3 = new S3(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY);

				$fileName = $file['name'];
				$fileTempName = $file['tmp_name'];
				// #todo Fix this chancher
				// I'm using a default file ext for simplicity.
				// $fileExt = pathinfo($fileName, PATHINFO_EXTENSION);
				$fileExt = SENDKICK_DEFAULT_IMAGE_EXT;
				$newName = "{$authBrand['username']}{$fileExt}";

				$s3->putBucket(AWS_SENDKICK_AVATARS_BUCKET, S3::ACL_PUBLIC_READ);
				$response = $s3->putObjectFile($fileTempName, AWS_SENDKICK_AVATARS_BUCKET, $newName, S3::ACL_PUBLIC_READ);

				unset($this->request->data['file']);
				$this->request->data['avatar'] = $newName;
			}

			$brand = Brands::first($authBrand['_id']);

			if ($brand->save($this->request->data))
			{
				FlashMessage::write('Has modificado tu perfil!');

				return true;
			}
			else
			{
				echo $this->errorsToList($brand->errors());
			}
		}

		// Create or edit a coupon, returns errors on model fail save.
		public function ajaxEditCoupon()
		{
			$this->_render['auto'] = false;

			$authBrand = Session::read('sendkick');

			if (trim($this->request->data['_id']))
			{
				$coupon = Coupons::editCoupon($authBrand['username'], $this->request->data);
			}
			else
			{
				$coupon = Coupons::newCoupon($authBrand['username'], $this->request->data);
			}

			if ($coupon->errors())
			{
				return $this->errorsToList($coupon->errors());
			}

			FlashMessage::write('Cupón listo!');

			return true;
		}

		// Delete a coupon
		public function ajaxDeleteCoupon()
		{
			$this->_render['auto'] = false;

			FlashMessage::write('Cupón eliminado!');

			return Coupons::deleteCoupon($this->request->data['_id']);
		}

		// Validate Code

		public function ajaxValidateCode()
		{
			$this->_render['auto'] = false;

			$authBrand = Session::read('sendkick');

			$status = Keys::getStatus($this->request->data['key'], $authBrand['username']);

			if ($status == 0)
			{
				echo '<ul><li>OK! El c&oacute;digo es v&aacute;lido!</li></ul>';

				return true;
			}

			if ($status == 1)
			{
				echo '<ul><li>El c&oacute;digo ya fue utilizado.</li></ul>';

				return true;
			}

			echo '<ul><li>Error: El c&oacute;digo no es v&aacute;lido.</li></ul>';

			return false;
		}

	}
?>