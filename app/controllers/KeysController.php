<?php

namespace app\controllers;

use app\models\Keys;

class KeysController extends \lithium\action\Controller
{

    public $publicActions = array(
        'apiUpdateKey',
        'apiIsKickedBy'
    );

    public function consume($key)
    {
        $data = array(
            '$set' => array(
                'status' => 1
            )
        );

        $conditions = array(
            'key' => (int) $key
        );

        Keys::update($data, $conditions, array('atomic' => false));

        $this->redirect('/brands');
    }

    public function apiUpdateKey()
    {
        $this->_render['auto'] = false;

        $couponId = $this->request->data['coupon_id'];

        $facebookId = $this->request->data['facebook_id'];

        $brand = $this->request->data['brand'];

        return Keys::toogleKey($couponId, $facebookId, $brand);
    }

    public function apiIsKickedBy()
    {
        $this->_render['auto'] = false;

        if (Keys::isKickedBy($this->request->data['coupon_id'], $this->request->data['facebook_id']))
        {
            echo 1;

            return true;
        }

        echo 0;

        return false;
    }

}

?>