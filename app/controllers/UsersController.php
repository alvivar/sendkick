<?php

	namespace app\controllers;

	use app\models\Users;
	use lithium\storage\Session;

	class UsersController extends \lithium\action\Controller
	{

		public $publicActions = array(
				'ajaxUpdateUser',
				'ajaxUpdateFBSession'
		);

		public function ajaxUpdateUser()
		{
			$this->_render['auto'] = false;

			$user = Users::find('first', array(
					'conditions' => array('facebook_id' => $this->request->data['facebook_id']),
					'fields' => array('facebook_id')
			));

			if (!$user)
			{
				$user = Users::create();
			}

			$user->created = time() . "";

			return $user->save($this->request->data);
		}

		public function ajaxUpdateFBSession()
		{
			$this->_render['auto'] = false;

			return Session::write('fbkick', array(
					'facebook_id' => $this->request->data['facebook_id'],
					'name' => $this->request->data['name'],
					'email' => $this->request->data['email']
			));
		}

	}
?>