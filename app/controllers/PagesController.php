<?php

namespace app\controllers;

use app\models\Coupons;
use app\models\Tags;

class PagesController extends \lithium\action\Controller
{

	public $publicActions = array('view');

	public function view()
	{
		$path = func_get_args() ? func_get_args() : array('home');

		if (in_array('home', $path))
		{
			$homeFeatured = Coupons::find('all', array(
					'order' => array('modified' => 'desc'),
					'limit' => 3
			));

			$homeToday = Coupons::find('all', array(
					'order' => array('modified' => 'desc'),
					'limit' => 2
			));

			$homeRecent = Coupons::find('all', array(
					'order' => array('modified' => 'desc'),
					'limit' => 2
			));

			$homeTags = Tags::find('all', array(
					'order' => array('searches' => 'desc'),
					'limit' => 36,
			));
		}

		return $this->render(array(
				'template' => join('/', $path),
				'data' => compact('homeFeatured', 'homeToday', 'homeRecent', 'homeTags')
		));
	}

}
?>