<?php

/**
 * li3_bitly plugin for Bitly integration.
 * Totally based on http://davidwalsh.name/bitly-api-php
 *
 * @license       http://opensource.org/licenses/bsd-license.php The BSD License
 */

namespace li3_bitly\extensions;

use lithium\core\Libraries;

class Bitlyfy extends \lithium\core\StaticObject
{

    // Wrapper methods

    public static function bitlyfy($url)
    {
        $config = Libraries::get('li3_bitly', 'config');
        
        return Bitlyfy::get_bitly_short_url($url, $config['user'], $config['key']);
    }

    public static function unbitlyfy($url)
    {
        $config = Libraries::get('li3_bitly', 'config');

        return Bitlyfy::get_bitly_long_url($url, $config['user'], $config['key']);
    }

    // Short url

    protected static function get_bitly_short_url($url, $login, $appkey, $format='txt')
    {
        $connectURL =
                'http://api.bit.ly/v3/shorten?login=' . $login .
                '&apiKey=' . $appkey .
                '&uri=' . urlencode($url) .
                '&format=' . $format;

        return Bitlyfy::curl_get_result($connectURL);
    }
    
    // Unshort url

    protected static function get_bitly_long_url($url, $login, $appkey, $format='txt')
    {
        $connectURL =
                'http://api.bit.ly/v3/expand?login=' . $login .
                '&apiKey=' . $appkey .
                '&shortUrl=' . urlencode($url) .
                '&format=' . $format;

        return Bitlyfy::curl_get_result($connectURL);
    }
    
    // CURL call to Bitly

    protected static function curl_get_result($connectUrl)
    {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $connectUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

}
?>