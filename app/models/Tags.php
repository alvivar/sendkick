<?php

namespace app\models;

class Tags extends \lithium\data\Model
{

    public $validates = array();

    // Add a new tag if doesn't exists, if exists add +1 to edits

    public static function addTag($newTag)
    {
        $tag = Tags::find('first', array(
                    'conditions' => array(
                        'name' => $newTag
                    ),
                    'fields' => array(
                        '_id'
                    )
                ));

        if (!$tag)
        {
            $tag = Tags::create();
            $tag->name = $newTag;
            $tag->edits = 0;
            $tag->searches = 0;
            $tag->random = mt_rand();
            $tag->created = time() . '';
            $tag->save();
        }
        else
        {
            Tags::pingEdit($newTag);
        }
    }

    // +1 on searches

    public static function pingEdit($tag)
    {
        $data = array(
            '$inc' => array('edits' => 1),
            '$set' => array('modified' => time() . ''));

        $conditions = array(
            'name' => $tag
        );

        $options = array('atomic' => false);


        Tags::update($data, $conditions, $options);
    }

    // +1 on searches

    public static function pingSearch($tag)
    {
        $data = array(
            '$inc' => array('searches' => 1),
            '$set' => array('modified' => time() . ''));

        $conditions = array(
            'name' => $tag
        );

        $options = array('atomic' => false);


        Tags::update($data, $conditions, $options);
    }

}

?>