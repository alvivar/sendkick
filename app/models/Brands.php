<?php

	namespace app\models;

	use lithium\util\Validator;
	use lithium\util\String;
	use lithium\util\Inflector;

	class Brands extends \lithium\data\Model
	{

		public $validates = array(
				'username' => array(
						array(
								'notEmpty',
								'message' => 'Tu nombre de usuario no puede ser vacío!'
						),
						array(
								'usernameRules',
								'message' => 'Tu nombre solo puede contener letras y números y debe iniciar con una letra!'
						),
						array(
								'uniqueUsername',
								'message' => 'Este nombre de usuario ya está registrado!'
						)
				),
				'name' => array( array(
							'notEmpty',
							'required' => false,
							'message' => 'Tu nombre no puede estar vacío!'
					)),
				'website' => array( array(
							'url',
							'skipEmpty' => true,
							'required' => false,
							'message' => 'Tu página web está mal escrita! Recuerda agregar "http://"'
					)),
				'facebook' => array( array(
							'url',
							'skipEmpty' => true,
							'required' => false,
							'message' => 'Tu dirección en Facebook está mal escrita! Recuerda agregar http://'
					)),
				'email' => array(
						array(
								'email',
								'message' => 'Tu email está mal escrito!'
						),
						array(
								'uniqueEmail',
								'message' => 'Este email ya está registrado!'
						)
				),
				'password_check' => array(
						array(
								'notEmpty',
								'message' => 'Tu contraseña no debe estar vacía!'
						),
						array(
								'email2xCheck',
								'message' => 'Las contraseñas no coinciden!'
						)
				)
		);

		// Change the password of a user by his email. Returns FALSE on error or the new password.

		public static function changePass($email)
		{
			$brand = Brands::find('first', array('conditions' => array('email' => $email)));

			$newPass = mt_rand(1000, 9999) . '';

			if ($brand)
			{
				$brand->password_check = $newPass;

				$brand->password = String::hash($newPass);

				if ($brand->save(null, array('validate' => false)))
				{
					return $newPass;
				}
				else
				{
					print_r($brand->errors());

					return false;
				}
			}

			return false;
		}

	}

	// Validations

	Validator::add('uniqueUsername', function($value, $format, $options)
	{
		// TRUE on _id already defined, means is a edit
		if (isset($options['values']['_id']))
		{
			return true;
		}

		$username = Brands::find('first', array(
				'conditions' => array('username' => $value),
				'fields' => array('_id')
		));

		if ($username)
		{
			return false;
		}

		return true;
	});

	Validator::add('uniqueEmail', function($value, $format, $options)
	{
		// TRUE on _id already defined, means is a edit
		if (isset($options['values']['_id']))
		{
			return true;
		}

		$email = Brands::find('first', array(
				'conditions' => array('email' => $value),
				'fields' => array('_id')
		));

		if ($email)
		{
			return false;
		}

		return true;
	});

	Validator::add('email2xCheck', function($value, $format, $options)
	{
		$password = $options['values']['password'];
		$password_check = $options['values']['password_check'];

		if ($password == String::hash($password_check))
		{
			return true;
		}

		return false;
	});

	Validator::add('emailExists', function($value, $format, $options)
	{
		$email = Brands::find('first', array(
				'conditions' => array('email' => $options['values']['email']),
				'fields' => array('_id')
		));

		if ($email)
		{
			return true;
		}

		return false;
	});

	Validator::add('usernameRules', function($value, $format, $options)
	{
		if (preg_match('/^[a-zA-Z][a-zA-Z0-9]*/', $value) && Inflector::slug($value, '') == $value)
		{
			return true;
		}

		return false;
	});

	Validator::add('usernameExists', function($value, $format, $options)
	{
		$username = Brands::find('first', array(
				'conditions' => array('username' => $value),
				'fields' => array('_id')
		));

		if ($username)
		{
			return true;
		}

		return false;
	});

	Validator::add('usernameVsPass', function($value, $format, $options)
	{
		$username = Brands::find('first', array(
				'conditions' => array(
						'username' => $value,
						'password' => String::hash($options['values']['password'])
				),
				'fields' => array('_id')
		));

		if ($username)
		{
			return true;
		}

		return false;
	});

	// Hash the password on save.
	Brands::applyFilter('save', function($self, $params, $chain)
	{
		$document = $params['entity'];

		if (!$document->_id)
		{
			$document->password = String::hash($document->password);
		}

		if (!empty($params['data']))
		{
			$document->set($params['data']);
		}

		$params['entity'] = $document;

		return $chain->next($self, $params, $chain);
	});
?>