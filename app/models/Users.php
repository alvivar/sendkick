<?php

namespace app\models;

class Users extends \lithium\data\Model
{

    public $validates = array();

    public static function createUser($data)
    {
        $data['created'] = time() . '';

        $user = Users::create();

        $user->save($data);

        return $user;
    }

}

?>