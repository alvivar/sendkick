<?php

namespace app\models;

use app\models\Coupons;

class Keys extends \lithium\data\Model
{

    public $validates = array();

    // Create a semi-unique key for a coupon and certain user.
    // Status: 0 = virgin & ready, 1 = consumed, 2 = deactivated
    // Return the key object.

    public static function toogleKey($couponId, $facebookId, $brand)
    {
        // Random "unique"

        $randomKey = mt_rand(99999, 999999);

        // Check

        $key = Keys::find('first', array(
                    'conditions' => array(
                        'coupon_id' => $couponId,
                        'facebook_id' => $facebookId
                    )
                ));

        if ($key)
        {
            if ($key->status == 0)
            {
                $key->status = 2;

                Coupons::incCoupon($couponId, -1);
            }
            else
            {
                $key->status = 0;
                $key->key = $randomKey;

                Coupons::incCoupon($couponId, 1);
            }

            $key->modified = time() . '';

            return $key->save();
        }

        // Key generation

        $key = Keys::create();
        $key->facebook_id = $facebookId;
        $key->brand = $brand;
        $key->coupon_id = $couponId;
        $key->key = $randomKey;
        $key->status = 0;
        $key->created = time() . '';

        if ($key->save())
        {
            Coupons::incCoupon($couponId, 1);
        }

        return $key;
    }

    // Return true if an FB user has clicked to status 0 on certain coupon.

    public static function isKickedBy($couponId, $facebookId)
    {
        $key = Keys::find('first', array(
                    'conditions' => array(
                        'coupon_id' => $couponId,
                        'facebook_id' => $facebookId,
                        'status' => 0
                    ),
                    'fields' => array(
                        '_id'
                    )
                ));

        if ($key)
        {
            return true;
        }

        return false;
    }

    // Check if a code is valid, return -1 on invalid, other

    public static function getStatus($key, $brand)
    {
        $theKey = Keys::find('first', array(
                    'conditions' => array(
                        'brand' => $brand,
                        'key' => (int) $key
                    )
                ));

        if ($theKey)
        {
            return $theKey->status;
        }

        return -1;
    }

}

?>