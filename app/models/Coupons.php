<?php

	namespace app\models;

	use lithium\util\Validator;
	use lithium\util\Inflector;
	use li3_bitly\extensions\Bitlyfy;

	class Coupons extends \lithium\data\Model
	{

		public $validates = array(
				'name' => array(
						'notEmpty',
						'message' => 'El nombre del cupón es obligatorio!',
						'required' => false
				),
				'description' => array(
						'notEmpty',
						'message' => 'La descripción del cupón es necesaria!'
				),
				'old_price' => array(
						array(
								'notEmpty',
								'message' => 'Necesitamos el precio normal! (Puede ser 0)'
						),
						array(
								'priceCheck',
								'message' => 'El precio de oferta debe ser menor al precio normal!'
						),
				),
				'new_price' => array( array(
							'notEmpty',
							'message' => 'Necesitamos el precio de oferta! (Puede ser 0)'
					))
		);

		// Create a new coupon for a brand. Returns false on error or the coupon object.
		public static function newCoupon($brand, $data)
		{
			unset($data['_id']);
			// It's new, so, we don't need an _id

			$data['created'] = time() . '';

			$coupon = Coupons::create();

			$coupon->brand = $brand;

			$coupon->save($data);

			return $coupon;
		}

		// Edit a coupon for a brand. Returns false on error or the coupon object.
		public static function editCoupon($brand, $data)
		{
			$coupon = Coupons::first($data['_id']);

			$coupon->save($data);

			return $coupon;
		}

		// Delete a coupon by _id
		public static function deleteCoupon($_id)
		{
			if (!$_id)
			{
				return false;
			}

			$coupon = Coupons::first($_id);

			return $coupon->delete();
		}

		// Inc/Dec coupon KICKS count.
		public static function incCoupon($couponId, $count = 1)
		{
			$data = array('$inc' => array('kicks' => $count));

			$conditions = array('_id' => $couponId);

			$options = array('atomic' => false);

			Coupons::update($data, $conditions, $options);
		}

	}

	// Change and fix a lot of things. Transform an clean data.
	Coupons::applyFilter('save', function($self, $params, $chain)
	{
		$document = $params['entity'];

		$data = $params['data'];

		// Slug

		$document->slug = strtolower(Inflector::slug($data['name']));

		// Short Url

		$url = SENDKICK_BITLY_URL_BASE . $document->brand . '/' . $document->slug;

		$document->url = trim(Bitlyfy::Bitlyfy($url));
		//$document->url = $url;

		// Tags parser

		$tags = array();

		foreach (explode(',', $data['tags']) as $t)
		{
			if (trim($t))
			{
				$tags[] = trim($t);
			}
		}

		$document->tags = $tags;

		unset($data['tags']);

		// The real tags for search

		$tagsSlug = array();

		foreach (explode('-', $document->slug) as $t)
		{
			if (trim($t))
			{
				$tagsSlug[] = trim($t);
			}
		}

		$document->tags_search = array_merge($tags, $tagsSlug);

		// Adding to Tags count

		foreach ($document->tags_search as $tag)
		{
			Tags::addTag($tag);
		}

		// Prices only can have numbers

		$data['old_price'] = (int)preg_replace("/[^0-9,.]/", "", $data['old_price']);

		$data['new_price'] = (int)preg_replace("/[^0-9,.]/", "", $data['new_price']);

		// Expiration

		$document->expiration_ut = @strtotime($data['expiration']) . '';

		// Misc

		$document->modified = time() . '';

		$document->random = mt_rand();

		// Reallocate data

		$params['entity'] = $document;

		$params['data'] = $data;

		return $chain->next($self, $params, $chain);
	});

	// Validations

	Validator::add('priceCheck', function($value, $format, $options)
	{

		$old_price = $options['values']['old_price'];
		$new_price = $options['values']['new_price'];

		if ($new_price >= $old_price)
		{
			return false;
		}

		return true;
	});
?>
