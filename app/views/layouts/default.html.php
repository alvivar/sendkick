<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">
	<head>
		<?php echo $this->html->charset();?>
		<title> SendKick (beta)<?php echo $this->title();?></title>
		<?php echo $this->html->style(array(
						'reset',
						'text',
						'960',
						'min.style',
						'ui-lightness/jquery-ui-1.8.13.custom'
				));
		?>
		<?php echo $this->scripts();?>
		<?php echo $this->html->link('Icon', null, array('type' => 'icon'));?>
		<?php echo $this->html->script(array(
					'jquery',
					'jquery-ui',
					'jquery.form'
			));
		?>
		<script src="http://connect.facebook.net/en_US/all.js"></script>
		<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
		<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
	</head>
	<body>
		<div id="main" class="container_16">
			<div id="fbHeader">
				<div id="fb-root"></div>
				<script>
					$(document).ready(function()
					{

						FB.init(
						{
							// Facebook connection
							appId : '214999841867079',
							cookie : true,
							status : true,
							xfbml : true
						});

						FB.getLoginStatus(function(response)
						{
							// On enter
							if(response.session)
							{
								hideLogin();
							}
							else
							{
								showLogin();
							}
						});

						$("#fbBtnLogin").click(function()
						{
							// Login button
							FB.login(function(response)
							{
								if(response.session)
								{
									if(response.perms)
									{
										// Logged & permissions
									}
									else
									{
										// Logged but no premissions
									}
								}
								else
								{
									// Not logged
								}
							},
							{
								perms : 'email,user_location'
							});
						});

						$("#fbBtnLogout").click(function()
						{
							// Logout from facebook
							FB.logout();
						});

						FB.Event.subscribe('auth.login', function(response)
						{
							// On login
							FB.api('/me', function(response)
							{
								$.post("/users/ajaxUpdateUser",
								{
									facebook_id : response.id,
									name : response.name,
									email : response.email,
									gender : response.gender,
									location : response.user_location
								});
							});
							hideLogin();
						});

						FB.Event.subscribe('auth.logout', function(response)
						{
							// On logout
							showLogin();
						});
						function showLogin()
						{
							// Show the login button
							$("#fbBtnLogin").show("fast");
							$("#fbBtnLogout").hide();
						}

						function hideLogin()
						{
							// Hide the login button
							$("#fbBtnLogin").hide();
							$("#fbBtnLogout").show("fast");
						}

					});

				</script>
				<div id="followSendKick">
					<ul>
						<li class="fb">
							<fb:like href="http://www.facebook.com/SendKick" send="false" layout="button_count" show_faces="false" font=""></fb:like>
						</li>
						<li class="gplus">
							<g:plusone size="medium" href="http://sendkick.com/"></g:plusone>
							<script type="text/javascript">
								(function()
								{
									var po = document.createElement('script');
									po.type = 'text/javascript';
									po.async = true;
									po.src = 'https://apis.google.com/js/plusone.js';
									var s = document.getElementsByTagName('script')[0];
									s.parentNode.insertBefore(po, s);
								})();

							</script>
						</li>
						<li class="twitter">
							<a href="https://twitter.com/sendkick" class="twitter-follow-button">Seguir @SendKick</a>
							<script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script>
						</li>
					</ul>
				</div>
				<a id="fbBtnLogout" style="display:none" href="#logout">salir</a>
				<a id="fbBtnLogin" style="display:none" href="#login"></a>
			</div>
			<div id="sendkickBay">
				<div id="sendkickBayLogo" class="grid_8">
					<a href="/"> <?= $this->html->image('home/logo-home.png');?></a>
				</div>
				<div id="sendkickBayInstructions" class="grid_8">
					<?= $this->html->image('home/instrucciones.png');?>
				</div>
				<div class="clear"></div>
			</div>
			<div class="grid_16">
				<div id="search" class="round10">
					<?= $this->html->image('base/busqueda-encontra-ofertas-promociones.png');?>
					<form method="get" action="/coupons/search/">
						<input id="searchBar" type="text" name="q" value="<?= isset($_GET['q']) ? $_GET['q'] : '';?>">
					</form>
				</div>
			</div>
			<div class="clear"></div>
			<div class="grid_16">
				<?= $this->flashMessage->output();?>
			</div>
			<div class="clear"></div>
			<div id="content">
				<?php echo $this->content();?>
				<div class="clear"></div>
			</div>
			<div id="aboutUs">
				<div class="vTitle">
					<?= $this->html->image('base/footer-borde-izquierdo.png');?>
				</div>
				<div id="aboutUsBg">
					<div>
						<?= $this->html->link('Términos de servicio', '/pages/toa');?>
					</div>
					<div>
						<?= $this->html->link('Contacto', '/pages/contact');?>
					</div>
					<div>
						<?= $this->html->link('SendKick para Empresas', '/brands');?>
					</div>
					<div class="auSpace">
						<?= $this->html->link('GekoMachine', 'http://www.gekomachine.com/');?>
					</div>
				</div>
				<div class="vTitle" style="float:right">
					<?= $this->html->image('base/footer-borde-derecho.png');?>
				</div>
			</div>
		</div>
	</body>
</html>
