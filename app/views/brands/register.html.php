<div class="grid_10 prefix_3">
    <div class="regForm">
        <?= $this->form->create($brand); ?>
        <?= $this->form->field('username', array('label' => 'Nombre de usuario')); ?>
        <?= $this->form->field('email', array('label' => 'Email')); ?>
        <?= $this->form->field('password_check', array('type' => 'password', 'label' => 'Contraseña')); ?>
        <?= $this->form->field('password', array('type' => 'password', 'label' => 'Repetir contraseña')); ?>
        <?= $this->form->submit('Registrarse'); ?>
        <?= $this->form->end(); ?>
    </div>
</div>