<div class="grid_10 prefix_3">
	<div class="regForm">
		<?= $this->form->create($brand);?>
		<?= $this->form->field('username', array('label' => 'Nombre de usuario'));?>
		<?= $this->form->field('password', array(
					'label' => 'Contraseña',
					'type' => 'password'
			));
		?>
		<?= $this->form->submit('Ingresar');?>
		<?php if ($brand->errors()):
		?>
		<br>
		<a id="regInvite" href="/brands/lost"><?= '¿Perdiste tu contraseña?';?></a>
		<?php endif;?>
		<br>
		<a id="regInvite" href="/brands/register">Registrate ac&aacute; si no tienes cuenta</a>
		<?= $this->form->end();?>
	</div>
</div>