<div class="grid_16">
	<div id="adminMenuLeft" class="adminBtns">
		<ul>
			<li>
				<a id="btnEditCoupon" href="#newCoupon">Nuevo cup&oacute;n</a>
			</li>
			<li>
				<a id="btnValidateCode" href="#validateCode">Validar c&oacute;digo</a>
			</li>
		</ul>
	</div>
	<div id="adminMenuRight" class="adminBtns">
		<ul>
			<li>
				<a id="btnEditProfile" href="#editProfile">Editar perfil</a>
			</li>
			<li>
				<a id="btnLogout" href="/brands/logout">Cerrar sesi&oacute;n</a>
			</li>
		</ul>
	</div>
</div>
<div class="clear"></div>
<div class="grid_16">
	<div class="theCupon">
		<div class="cupon">
			<div class="avatar">
				<a href="/brands/show/<?= $brand->username;?>"> <?= $this->html->image(AWS_SENDKICK_AVATARS_URL . $brand->username . SENDKICK_DEFAULT_IMAGE_EXT, array(
						'width' => 144,
						'height' => 144
				));
				?></a>
			</div>
			<div class="name">
				<?= $brand->name;?>
			</div>
			<div class="desc">
				<?= $brand->description;?>
			</div>
		</div>
		<div class="cuponSocial">
			<?php if (trim($brand->website)):
			?>
			<div>
				<?php
				$website = strpos($brand->website, 'http');
				?>
				Web: <a href="<?= $brand->website;?>"><?= $brand->website;?></a>
			</div>
			<?php endif;?>
			<?php if (trim($brand->twitter)):
			?>
			<div>
				<a href="https://twitter.com/<?= $brand->twitter;?>" class="twitter-follow-button">Seguir @<?= $brand->twitter;?></a>
				<script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script>
			</div>
			<?php endif;?>
			<?php if (trim($brand->facebook)):
			?>
			<div>
				<fb:like href="<?= $brand->facebook;?>" send="false" layout="button_count" width="450" show_faces="false" font=""></fb:like>
			</div>
			<?php endif;?>
			<?php if (trim($brand->website)):
			?>
			<div>
				<g:plusone href="<?= $brand->website;?>" size="medium"></g:plusone>
				<script type="text/javascript">
					(function()
					{
						var po = document.createElement('script');
						po.type = 'text/javascript';
						po.async = true;
						po.src = 'https://apis.google.com/js/plusone.js';
						var s = document.getElementsByTagName('script')[0];
						s.parentNode.insertBefore(po, s);
					})();

				</script>
			</div>
			<?php endif;?>
		</div>
	</div>
</div>
<div class="clear"></div>
<div class="grid_16">
	<div id="couponsList" >
		<div class="vTitle">
			<?= $this->html->image('home/recientes-borde-izquierdo.png');?>
		</div>
		<div class="theTickets">
			<?php foreach ($coupons as $coupon):
			?>
			<div class="ticket">
				<div class="jsonMessage" style="display:none">
					<?= json_encode($coupon->data());?>
				</div>
				<div class="square1">
					<a class="">Editar</a>
					<a class="btnReloadCoupon" style="display:none" href="#edit">EDITAR</a>
					<div class="avatar">
						<?= $this->html->image(AWS_SENDKICK_AVATARS_URL . $coupon->brand . SENDKICK_DEFAULT_IMAGE_EXT, array(
								'width' => 50,
								'height' => 50
						));
						?>
					</div>
					<div class="buttonGoKick">
						<?= $this->html->link('Ver cupón', "/coupons/show/{$coupon->brand}/{$coupon->slug}");?>
					</div>
				</div>
				<div class="square3">
					<div class="name">
						<?= $coupon->name;?>
					</div>
					<div class="description">
						<?= trim(substr($coupon->description, 0, SENDKICK_MINI_COUPON_SIZE));?>
						...
					</div>
				</div>
			</div>
			<?php endforeach;?>
		</div>
	</div>
</div>
<div class="clear"></div>
<div id="dlgEditProfile" title="Datos" style="display:none">
	<?= $this->form->create($brand, array(
			'url' => 'brands/ajaxEditProfile',
			'id' => 'frmEditProfile'
	));
	?>
	<div class="frmErrors"></div>
	<div>
		<label>Avatar</label>
		<input id="file" type="file" name="file">
	</div>
	<?= $this->form->field('name', array('label' => 'Nombre'));?>
	<?= $this->form->field('description', array(
				'type' => 'textarea',
				'label' => 'Descripción'
		));
	?>
	<?= $this->form->field('website', array('label' => 'Página web'));?>
	<?= $this->form->field('facebook', array('label' => 'Facebook URL'));?>
	<?= $this->form->field('twitter', array('label' => 'Twitter ID'));?>
	<?= $this->form->submit('Guardar');?>
	<?= $this->form->end();?>
</div>
<div id="dlgEditCoupon" title="Cup&oacute;n" style="display:none">
	<?= $this->form->create(null, array(
			'url' => 'brands/ajaxEditCoupon',
			'id' => 'frmEditCoupon'
	));
	?>
	<div class="frmErrors"></div>
	<div style="float:left;">
		<?= $this->form->hidden('_id');?>
		<?= $this->form->field('name', array('label' => 'Nombre'));?>
		<?= $this->form->field('description', array(
					'type' => 'textarea',
					'label' => 'Descripción'
			));
		?>
		<?= $this->form->field('old_price', array('label' => 'Precio normal'));?>
		<?= $this->form->field('new_price', array('label' => 'Precio oferta'));?>
	</div>
	<div style="float: right;">
		<?= $this->form->field('limit', array('label' => 'Límite'));?>
		<?= $this->form->field('expiration', array(
					'id' => 'datepicker',
					'label' => 'Expira'
			));
		?>
		<?= $this->form->field('tags', array('label' => 'Tags'));?>
		<?= $this->form->field('rules', array(
					'type' => 'textarea',
					'label' => 'Restricciones'
			));
		?>
	</div>
	<a class="btnDeleteCouponLock" href="#delete">borrar</a>
	<a class="btnDeleteCoupon" href="#delete" style="display:none">Confimar</a>
	<a class="btnDeleteCouponCancel" href="#delete" style="display:none">Cancelar</a>
	<?= $this->form->submit('Guardar');?>
	<div style="clear:both"></div>
	<?= $this->form->end();?>
</div>
<div id="dlgValidateCode" title="C&oacute;digo" style="display:none">
	<?= $this->form->create(null, array(
			'url' => 'brands/ajaxValidateCode',
			'id' => 'frmValidateCode'
	));
	?>
	<div class="frmErrors"></div>
	<?= $this->form->field('key', array('label' => 'Código'));?>
	<?= $this->form->hidden('consume', array('value' => 0));?>
	<?= $this->form->submit('Verificar');?>
	<?= $this->html->link('Consumir', "#", array('style' => 'display:none'));?>
	<div style="clear:both"></div>
	<?= $this->form->end();?>
</div>
<div id="lostAndFound" style="display:none">
	<?= $this->html->image('loader.gif');?>
</div>
<script src="/js/brands.index.js"></script>