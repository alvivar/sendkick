<div class="grid_16">
	<div class="theCupon">
		<div class="cupon">
			<div class="avatar">
				<a href="/brands/show/<?= $brand->username;?>"> <?= $this->html->image(AWS_SENDKICK_AVATARS_URL . $brand->username . SENDKICK_DEFAULT_IMAGE_EXT, array(
						'width' => 144,
						'height' => 144
				));
				?></a>
			</div>
			<div class="name">
				<?= $brand->name;?>
			</div>
			<div class="desc">
				<?= $brand->description;?>
			</div>
		</div>
		<div class="cuponSocial">
			<?php if (trim($brand->website)):
			?>
			<div>
				<?php
				$website = strpos($brand->website, 'http');
				?>
				Web: <a href="<?= $brand->website;?>"><?= $brand->website;?></a>
			</div>
			<?php endif;?>
			<?php if (trim($brand->twitter)):
			?>
			<div>
				<a href="https://twitter.com/<?= $brand->twitter;?>" class="twitter-follow-button">Seguir @<?= $brand->twitter;?></a>
				<script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script>
			</div>
			<?php endif;?>
			<?php if (trim($brand->facebook)):
			?>
			<div>
				<fb:like href="<?= $brand->facebook;?>" send="false" layout="button_count" width="450" show_faces="false" font=""></fb:like>
			</div>
			<?php endif;?>
			<?php if (trim($brand->website)):
			?>
			<div>
				<g:plusone href="<?= $brand->website;?>" size="medium"></g:plusone>
				<script type="text/javascript">
					(function()
					{
						var po = document.createElement('script');
						po.type = 'text/javascript';
						po.async = true;
						po.src = 'https://apis.google.com/js/plusone.js';
						var s = document.getElementsByTagName('script')[0];
						s.parentNode.insertBefore(po, s);
					})();

				</script>
			</div>
			<?php endif;?>
		</div>
	</div>
</div>
<div class="clear"></div>
<div class="grid_16">
	<div id="couponsList" >
		<div class="vTitle">
			<?= $this->html->image('home/recientes-borde-izquierdo.png');?>
		</div>
		<div class="theTickets">
			<?php foreach ($coupons as $coupon):
			?>
			<div class="ticket">
				<div class="jsonMessage" style="display:none">
					<?= json_encode($coupon->data());?>
				</div>
				<div class="square1">
					<!-- <a class="category"></a>-->
					<a class="btnReloadCoupon" style="display:none" href="#edit">cliquea ac&aacute; para editar
					<br/>
					!</a>
					<div class="avatar">
						<a href="/brands/show/<?= $coupon->brand;?>"> <?= $this->html->image(AWS_SENDKICK_AVATARS_URL . $coupon->brand . SENDKICK_DEFAULT_IMAGE_EXT, array(
								'width' => 50,
								'height' => 50
						));
						?></a>
					</div>
					<div class="buttonGoKick">
						<?= $this->html->link('Ver cupón', "/coupons/show/{$coupon->brand}/{$coupon->slug}");?>
					</div>
				</div>
				<div class="square3">
					<div class="name">
						<?= $coupon->name;?>
					</div>
					<div class="description">
						<?= trim(substr($coupon->description, 0, SENDKICK_MINI_COUPON_SIZE));?>
						...
					</div>
				</div>
			</div>
			<?php endforeach;?>
		</div>
	</div>
</div>
<div class="clear"></div>
