<div id="searchView">
	<div class="grid_16">
		<div class="homeFeatured round10">
			<div class="homeFeaturedCupons">
				<?php $end = 3;?>
				<?php foreach ($coupons as $coupon):
				?>
				<?php
				if ($end-- <= 0)
				{
					break;
				}
				?>
				<div class="feature">
					<div class="Type">
						<div class="TypeName">
							<?= trim(substr($coupon->name, 0, 15));?>
							...
						</div>
						<div class="TypeTag">
							<a><?= "[TAG]";?></a>
						</div>
					</div>
					<div class="Avatar">
						<a href="/brands/show/<?= $coupon->brand;?>"> <?= $this->html->image(AWS_SENDKICK_AVATARS_URL . $coupon->brand . SENDKICK_DEFAULT_IMAGE_EXT, array(
								'width' => 50,
								'height' => 50
						));
						?></a>
					</div>
					<div class="Desc">
						<?= trim(substr($coupon->description, 0, SENDKICK_COUPON_SIZE));?>
						...
					</div>
					<div class="Details">
						<div class="DetailsInfo">
							<?php
							$avalaible = $coupon->limit - $coupon->kicks;
							?>
							<?= "Cupones disponibles: {$avalaible}"
							?><br/>
							<?= "Válido hasta: {$coupon->expiration}"
							?>
						</div>
						<div class="DetailsGo buttonGoKick">
							<?= $this->html->link('KICK', "/coupons/show/{$coupon->brand}/{$coupon->slug}");?>
						</div>
					</div>
				</div>
				<?php endforeach;?>
			</div>
		</div>
	</div>
	<div class="grid_16">
		<div id="couponsList" >
			<div class="vTitle">
				<?= $this->html->image('home/recientes-borde-izquierdo.png');?>
			</div>
			<div class="theTickets">
				<?php foreach ($coupons as $coupon):
				?>
				<div class="ticket">
					<div class="square1">
						<a class="category"><?= "";?></a>
						<div class="avatar">
							<a href="/brands/show/<?= $coupon->brand;?>"> <?= $this->html->image(AWS_SENDKICK_AVATARS_URL . $coupon->brand . SENDKICK_DEFAULT_IMAGE_EXT, array(
									'width' => 50,
									'height' => 50
							));
							?></a>
						</div>
						<div class="buttonGoKick">
							<?= $this->html->link('KICK', "/coupons/show/{$coupon->brand}/{$coupon->slug}");?>
						</div>
					</div>
					<div class="square3">
						<div class="name">
							<?= $coupon->name;?>
						</div>
						<div class="description">
							<?= trim(substr($coupon->description, 0, SENDKICK_MINI_COUPON_SIZE));?>
							...
						</div>
					</div>
				</div>
				<?php endforeach;?>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function()
	{
		$("#searchBar").focus();
	});

</script>