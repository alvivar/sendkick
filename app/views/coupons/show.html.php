<script>
	$(document).ready(function() {

// Init, call to test if already clicked by user

FB.getLoginStatus(function(response) {

if (response.session) {

FB.api('/me', function(response) {

$.post("/keys/apiIsKickedBy", {

coupon_id: $("#KickButton").parent().attr("id"),
facebook_id: response.id

}, function(response) {

if (parseInt(response) == 1) {
$("#KickButton").addClass("kicked");
}

});

});
}
});

// KICK button for the FB user with the coupon_id

$("#KickButton").click(function() {
$(this).html("...");
kickButtonCicle($(this), $(this).parent().attr("id"), "<?= $coupon->brand;?>
	");
	});

	function kickButtonCicle(div, coupon_id, brand)
	{
		FB.getLoginStatus(function(response)
		{

			if(response.session)
			{

				// Update Session & create ticket

				FB.api('/me', function(response)
				{

					$.post("/users/ajaxUpdateFBSession",
					{
						facebook_id : response.id,
						name : response.name,
						email : response.email
					});

					$.post("/keys/apiUpdateKey",
					{
						facebook_id : response.id,
						coupon_id : coupon_id,
						brand : brand
					}, function(response)
					{

						if(div.hasClass("kicked"))
						{
							$("#kicksCount").html(parseInt($("#kicksCount").html()) - 1 + "");
						}
						else
						{
							$("#kicksCount").html(parseInt($("#kicksCount").html()) + 1 + "");
						}

						div.html("Codigo generado");
						div.toggleClass("kicked");

					});
				});
			}
			else
			{

				// Relog an try again on logged

				FB.login(function(response)
				{
					if(response.session)
					{
						if(response.perms)
						{
							kickButtonCicle(div, coupon_id);
						}
						else
						{
							kickButtonCicle(div, coupon_id);
						}
					}
					else
					{
						alert("Necesitas loguearte con Facebook para usar SendKick!")
					}
				},
				{
					perms : 'email,user_location'
				});

			}
		});
	}

	});
</script>
<div class="grid_16">
	<div class="theCupon">
		<div class="cupon">
			<div class="avatar">
				<a href="/brands/show/<?= $coupon->brand;?>"> <?= $this->html->image(AWS_SENDKICK_AVATARS_URL . $coupon->brand . SENDKICK_DEFAULT_IMAGE_EXT, array(
						'width' => 144,
						'height' => 144
				));
				?></a>
			</div>
			<div class="type">
				<!-- <div class="tag">
					<a href="/search/"></a>
				</div> -->
				<div class="info">
					<?php $avalaible = $coupon->limit - $coupon->kicks;?>
					<?= "Cupones disponibles: {$avalaible}";?>
					<br/>
					<?= "Válido hasta: {$coupon->expiration}";?>
				</div>
			</div>
			<div class="name">
				<?= $coupon->name;?>
			</div>
			<div class="desc">
				<?= $coupon->description;?>
			</div>
			<div class="price">
				<?php
				$percentage = $coupon->new_price * 100 / $coupon->old_price;
				$saving = $coupon->old_price - $coupon->new_price;
				?>
				Precio&#58; <?= '₡' . $coupon->new_price;?>
				<br/>
				Descuento&#58; <?= '%' . (100 - floor($percentage));?>
				<br/>
				Te ahorras&#58; <?= '₡' . $saving;?>
			</div>
			<div id="<?= $coupon->_id;?>" class="kickBtn">
				<div>
					<span id="kicksCount"><?= $coupon->kicks ? : 0;
						?></span> / <?= $coupon->limit;?>
				</div>
				<a id="KickButton" class="round5" href="#kick">Generar Codigo</a>
			</div>
		</div>
		<div class="cuponSocial">
			<div>
				Enlace corto: <?= $this->html->link($coupon->url, $coupon->url);?>
			</div>
			<div>
				<fb:like href="<?= $coupon->url;?>" send="false" layout="button_count" width="450" show_faces="false" font=""></fb:like>
			</div>
			<div>
				<a href="http://twitter.com/share" class="twitter-share-button" data-url="<?= $coupon->url;?>" data-text="<?= $coupon->name;?>" data-count="horizontal" data-via="sendkick" data-lang="es">Tweet</a>
			</div>
			<div>
				<g:plusone size="medium"></g:plusone>
				<script type="text/javascript">
					(function()
					{
						var po = document.createElement('script');
						po.type = 'text/javascript';
						po.async = true;
						po.src = 'https://apis.google.com/js/plusone.js';
						var s = document.getElementsByTagName('script')[0];
						s.parentNode.insertBefore(po, s);
					})();

				</script>
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>
<div class="grid_16">
	<div id="couponsList" >
		<div class="vTitle">
			<?= $this->html->image('home/recientes-borde-izquierdo.png');?>
		</div>
		<div class="theTickets">
			<?php foreach ($coupons as $coupon):
			?>
			<div class="ticket">
				<div class="square1">
					<!--<a class="category"><?= "";?></a>-->
					<div class="avatar">
						<a href="/brands/show/<?= $coupon->brand;?>"> <?= $this->html->image(AWS_SENDKICK_AVATARS_URL . $coupon->brand . SENDKICK_DEFAULT_IMAGE_EXT, array(
								'width' => 50,
								'height' => 50
						));
						?></a>
					</div>
					<div class="buttonGoKick">
						<?= $this->html->link('Ver Cupón', "/coupons/show/{$coupon->brand}/{$coupon->slug}");?>
					</div>
				</div>
				<div class="square3">
					<div class="name">
						<?= $coupon->name;?>
					</div>
					<div class="description">
						<?= trim(substr($coupon->description, 0, SENDKICK_MINI_COUPON_SIZE));?>
						...
					</div>
				</div>
			</div>
			<?php endforeach;?>
		</div>
	</div>
</div>