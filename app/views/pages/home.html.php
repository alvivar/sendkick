<div class="grid_16">
	<div class="homeFeatured round10">
		<div class="homeFeaturedCupons">
			<?php foreach ($homeFeatured as $coupon):
			?>
			<div class="feature">
				<div class="Type">
					<div class="TypeName">
						<?= trim(substr($coupon->name, 0, 15));?>
						...
					</div>
					<!-- <div class="TypeTag">
						<a href="#"><?= "[TAG]";?></a>
					</div> -->
				</div>
				<div class="Avatar">
					<a href="/brands/show/<?= $coupon->brand;?>"> <?= $this->html->image(AWS_SENDKICK_AVATARS_URL . $coupon->brand . SENDKICK_DEFAULT_IMAGE_EXT, array(
							'width' => 50,
							'height' => 50
					));
					?></a>
				</div>
				<div class="Desc">
					<?= trim(substr($coupon->description, 0, SENDKICK_COUPON_SIZE));?>
					...
				</div>
				<div class="Details">
					<div class="DetailsInfo">
						<?php
						$avalaible = $coupon->limit - $coupon->kicks;
						?>
						<?= "Cupones disponibles: {$avalaible}"
						?><br/>
						<?= "Válido hasta: {$coupon->expiration}"
						?>
					</div>
					<div class="DetailsGo buttonGoKick">
						<?= $this->html->link('Ver cupón', "/coupons/show/{$coupon->brand}/{$coupon->slug}", array('class' => 'kickBtn'));?>
					</div>
				</div>
			</div>
			<?php endforeach;?>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>
<div class="grid_16">
	<div id="homeTags">
		<div class="vTitle">
			<?= $this->html->image('home/categorias-borde-izquierdo.png');?>
		</div>
		<?php

		$jump = 0;
		$count = 0;

		echo '<ul>';
		foreach ($homeTags as $tag)
		{
			echo '<li>', $this->html->link($tag->name, "/coupons/search/?q={$tag->name}"), '</li>';
			$count++;

			if ($jump++ >= 5)
			{
				$jump = 0;

				if ($count < 36)
				{
					echo '</ul>', '<div class="homeTagSpacer">', $this->html->image('home/categorias-separador.png'), '</div>', '<ul>';
				}
				else
				{
					echo '</ul>', '<ul>';
				}
			}
		}
		echo '</ul>';
		?>
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>
<div class="grid_8">
	<div id="homeToday" >
		<div class="vTitle">
			<?= $this->html->image('home/vencen-hoy-borde-izquierdo.png');?>
		</div>
		<?php foreach ($homeToday as $coupon):
		?>
		<div class="ticket">
			<div class="square1">
				<!-- <a class="category"><?= "";?></a> -->
				<div class="avatar">
					<a href="/brands/show/<?= $coupon->brand;?>"> <?= $this->html->image(AWS_SENDKICK_AVATARS_URL . $coupon->brand . SENDKICK_DEFAULT_IMAGE_EXT, array(
							'width' => 50,
							'height' => 50
					));
					?></a>
				</div>
				<div class="buttonGoKick">
					<?= $this->html->link('Ver cupón', "/coupons/show/{$coupon->brand}/{$coupon->slug}");?>
				</div>
			</div>
			<div class="square3">
				<div class="name">
					<?= $coupon->name;?>
				</div>
				<div class="description">
					<?= trim(substr($coupon->description, 0, SENDKICK_MINI_COUPON_SIZE));?>
					...
				</div>
			</div>
		</div>
		<?php endforeach;?>
		<div class="clear"></div>
	</div>
</div>
<div class="grid_8">
	<div id="homeRecent">
		<div class="vTitle">
			<?= $this->html->image('home/recientes-borde-izquierdo.png');?>
		</div>
		<?php foreach ($homeRecent as $coupon):
		?>
		<div class="ticket">
			<div class="square1">
				<!-- <a class="category"><?= "";?></a> -->
				<div class="avatar">
					<a href="/brands/show/<?= $coupon->brand;?>"> <?= $this->html->image(AWS_SENDKICK_AVATARS_URL . $coupon->brand . SENDKICK_DEFAULT_IMAGE_EXT, array(
							'width' => 50,
							'height' => 50
					));
					?></a>
				</div>
				<div class="buttonGoKick">
					<?= $this->html->link('Ver cupón', "/coupons/show/{$coupon->brand}/{$coupon->slug}");?>
				</div>
			</div>
			<div class="square3">
				<div class="name">
					<?= $coupon->name;?>
				</div>
				<div class="description">
					<?= trim(substr($coupon->description, 0, SENDKICK_MINI_COUPON_SIZE));?>
					...
				</div>
			</div>
		</div>
		<?php endforeach;?>
		<div class="clear"></div>
	</div>
</div>
